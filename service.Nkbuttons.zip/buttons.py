#!/usr/bin/pyt


import sys
sys.path.append('/storage/.kodi/addons/virtual.rpi-tools/lib')

import RPi.GPIO as GPIO
import xbmc

upPin = 10      # board pin 11


def up_callback(channel):
    xbmc.executebuiltin("Action(PlayPause)")


class Main:
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(upPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    GPIO.add_event_detect(upPin, GPIO.FALLING, callback=up_callback, bouncetime=300)

    while not xbmc.abortRequested:
        xbmc.sleep(5)
        
class TidyUp:
    GPIO.remove_event_detect(upPin)

    GPIO.cleanup(upPin)

    
if (__name__ == "__main__"):
    Main()
    TidyUp()
